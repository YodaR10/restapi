﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DisputeRestApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DisputeController : ControllerBase
    {

        private static readonly string nonUsata = "nonUsata";
        private readonly ILogger<DisputeController> _logger;

        public DisputeController(ILogger<DisputeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Dispute> Get()
        {
            return new Dispute[] { new Dispute() { GcoNumber = "GCO01", Description = "test" } };
        }

        [HttpGet("{id}")]
        public IEnumerable<Dispute> Get(string id)
        {
            if (id == "a")
                throw new Exception("a");
            else if (id == "b")
                throw new Exception("a");

            return new Dispute[] { new Dispute() { GcoNumber = id, Description = "test" } };
        }

    }
}
